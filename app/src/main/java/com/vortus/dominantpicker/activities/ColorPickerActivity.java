/*
Čia vyksta color pickeris
 */

package com.vortus.dominantpicker.activities;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.vortus.dominantpicker.R;
import net.margaritov.preference.colorpicker.ColorPickerDialog;

import extensions.Extensions;

public class ColorPickerActivity extends AppCompatActivity {

    // Kintamieji
    private ColorPickerDialog colorPickerDialog;
    private TextView colorCodeView;
    private RelativeLayout layout;
    private int selectedColor = Color.parseColor("#ffffff"); // Pradinė pasirinkta spalva

    @Override
    protected void onCreate(Bundle savedInstanceState) { // Kai pasileidžia puslapis
        super.onCreate(savedInstanceState);
        // Padarom Fullscreen
        Extensions.setFullscreen(this);
        // Uždedam ikoną
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        setContentView(R.layout.activity_color_picker);

        colorCodeView = (TextView)findViewById(R.id.colorCodeText); // Atsakymų laukas
        layout = (RelativeLayout)findViewById(R.id.relativeLayout); // Susirandam pagrindinį išsidėstymą dėl fono spalvos pakeitimo

        final ClipboardManager clipboardManager = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE); // Susikuriam iškirptės valdiklį
        Button buttonOpenDialog = (Button)findViewById(R.id.openColorDialog); // Susirandam atsidarymo knopkę

        buttonOpenDialog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) { // Jei paspaudžia spalvos pasirinkimo knopkę
                colorPickerDialog = new ColorPickerDialog(ColorPickerActivity.this, selectedColor); // Sukuriam spalvos pasirinkimo dialogą
                colorPickerDialog.setTitle("Pasirinkite spalvą"); // Dialogo pavadinimas
                colorPickerDialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener() {
                    @Override
                    public void onColorChanged(int i) { // Jei pakeitė spalvą
                        selectedColor = i;
                        String hex = Integer.toHexString(selectedColor); // Surandam hex value
                        colorCodeView.setText("HEX Kodas: #" + hex.toUpperCase().substring(2)); // Uždedam rezultatą
                        layout.setBackgroundColor(selectedColor); // Pakeičiam fono spalvą
                        clipboardManager.setText("#" + hex.toUpperCase().substring(2)); // Išsaugom iškirptėje
                        Toast.makeText(ColorPickerActivity.this, "Kodas nukopijuotas į iškirptę", Toast.LENGTH_SHORT).show(); // Sukuriam žinutę dėl iškirptės
                    }
                });
                colorPickerDialog.show(); // Pradedam rodyti dialogą
            }
        });
    }

}
