/*
Čia yra pati pradžia visko.
 */

package com.vortus.dominantpicker.activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.vortus.dominantpicker.R;

import extensions.Extensions;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Padarom Fullscreen
        Extensions.setFullscreen(this);
        // Uždedam ikoną
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        setContentView(R.layout.activity_main);
    }

    public void chooseColorClick(View v){ // Atidarom spalvos pasirinkimą, jei paspaudė
        startActivity(new Intent(MainActivity.this, ColorPickerActivity.class));
    }

    public void findDominantClick(View v){ // Atidarom dominuojančias spalvas, jei paspaudė
        startActivity(new Intent(MainActivity.this, FindDominantActivity.class));
    }

}
