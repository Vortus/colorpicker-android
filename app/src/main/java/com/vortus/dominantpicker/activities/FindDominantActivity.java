package com.vortus.dominantpicker.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.vortus.dominantpicker.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import extensions.Extensions;
import extensions.ImageHandler;
import extensions.BitmapColoring;
import extensions.Variables;

public class FindDominantActivity extends AppCompatActivity {

    public static final int IMAGE_GALLERY_REQUEST = 666; // Unikalus kodas galerijos iškvietimui
    // Kintamieji
    private Button buttonOpenDialog, buttonA, buttonB, buttonC;
    private ImageHandler imageHandler; // Paveikslėlio valdiklis
    private ImageView imageView;
    private Bitmap imageBitmap;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Inicializacija
        // Padarom Fullscreen
        Extensions.setFullscreen(this);
        // Uždedam ikoną
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        setContentView(R.layout.activity_find_dominant);

        imageView = (ImageView)findViewById(R.id.imageView); // Surandam paveikslėlio rodymo lauką
        buttonOpenDialog = (Button)findViewById(R.id.buttonSelectImageFile); // Surandam knopkę
        buttonA = (Button)findViewById(R.id.buttonA);
        buttonB = (Button)findViewById(R.id.buttonB);
        buttonC = (Button)findViewById(R.id.buttonC);
    }

    // Paspaudus Atidaryti paveikslėlių galeriją
    public void buttonOpenDialogClick(View view){
        // Paveikslėlių galerija, Intent - tai yra android sisteminė operacija, kuri gali atidaryti kitas pagrindines operacijas kaip skambinti arba pvz paveikslėlių galerija
        // Apie Intent daugiau http://www.tutorialspoint.com/android/android_intents_filters.htm
        Intent photoPicker = new Intent(Intent.ACTION_PICK); // Veiksmas bus rinktis

        // Kur yra ta paveikslėlių galerija, gaunam su androido sistemine pagalba
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String path = pictureDirectory.getPath(); // Gaunam kelią

        // Gaunam kaip Uri - universalus adresas sd kortelėje
        Uri data = Uri.parse(path);

        // Koks tipas ir kas turi būti failas
        photoPicker.setDataAndType(data, "image/*");

        // Atidarom galerija ir gaunam is jos paveikslėlį
        startActivityForResult(photoPicker, IMAGE_GALLERY_REQUEST);
    }

    @Override // Gaunam activity result ir tikrinam request kodą ar jis yra galerijos
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            // Viska ok
            if(requestCode == IMAGE_GALLERY_REQUEST){ // Ar iškvietė galerija
                Uri imageUri = data.getData(); // Gaunam adresa

                // Reikia nuskaitymui paveikslėlio iš uri adreso
                try { // Jei kažkas atsitiks bandom, pvz neras paveikslėlio nes ištrauksi sd kortą ir pnš...
                    InputStream inputStream = getContentResolver().openInputStream(imageUri);

                    // Gaunam bitmapa iš streamo
                    Bitmap imageBitmap = BitmapFactory.decodeStream(inputStream);

                    try { // Bandom iškviesti paveikslėlio valdiklį

                                // Sumažinam paveikslėlį arba padidinam jei labai mažas
                                // Jei didinam labai mažą tai vistiek gaunasi apskaičiavimas gerai
                                // Kadangi ir padaugėja vienodų spalvų
                                int nw = (int) (imageBitmap.getHeight() * (512.0 / imageBitmap.getWidth()));
                                Bitmap scaledBitmap = Bitmap.createScaledBitmap(imageBitmap, 512, nw, true);
                                imageView.setImageBitmap(BitmapColoring.getRoundedCornerBitmap(scaledBitmap, 10));

                                // Skaičiuojam trispalvę
                                imageHandler = new ImageHandler(scaledBitmap); // Pradedam skaičiuoti trispalvę

                                // Sudedam spalvas
                                buttonA.setBackgroundColor(imageHandler.getColor(0));
                                buttonB.setBackgroundColor(imageHandler.getColor(1));
                                buttonC.setBackgroundColor(imageHandler.getColor(2));

                                // Jei spalvos per tamsios uždedam baltą textą
                                buttonA.setText(BitmapColoring.getHex(imageHandler.getColor(0))); // Uždedam textą
                                int lumA = (int)BitmapColoring.getLum(imageHandler.getColor(0));
                                if(lumA < Variables.MAXDARKNESS) // Jei tamsi spalva
                                    buttonA.setTextColor(Color.WHITE); // Perkeičiam texto spalvą kad būtu matoma

                                buttonB.setText(BitmapColoring.getHex(imageHandler.getColor(1))); // Uždedam textą
                                int lumB = (int)BitmapColoring.getLum(imageHandler.getColor(1));
                                if(lumB < Variables.MAXDARKNESS) // Jei tamsi spalva
                                    buttonB.setTextColor(Color.WHITE); // Perkeičiam texto spalvą kad būtu matoma

                                buttonC.setText(BitmapColoring.getHex(imageHandler.getColor(2))); // Uždedam textą
                                int lumC = (int)BitmapColoring.getLum(imageHandler.getColor(2));
                                if(lumC < Variables.MAXDARKNESS) // Jei tamsi spalva
                                    buttonC.setTextColor(Color.WHITE); // Perkeičiam texto spalvą kad būtu matoma

                                // Parodom knopkes
                                buttonA.setVisibility(View.VISIBLE);
                                buttonB.setVisibility(View.VISIBLE);
                                buttonC.setVisibility(View.VISIBLE);

                    } catch (Exception e) { // Jei įvyko klaida skaičiuojant trispalvę
                        Toast.makeText(this, "Klaida apdorojant paveikslėlį", Toast.LENGTH_LONG);
                    }

                } catch (FileNotFoundException e) { // Jei nerado failo
                    e.printStackTrace();
                    Toast.makeText(this, "Negalima atidaryti paveikslėlio", Toast.LENGTH_LONG);
                }
            }
        }
        //--
    }
}
