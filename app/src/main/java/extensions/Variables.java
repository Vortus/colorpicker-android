/*
Statiški kintamieji, t.y gali būti panaudoti betkur
 */

package extensions;

public class Variables {

    public static final int HEXSTART = -5000, HEXEND = -16777216; // ~#FFFFFF iki ~#000000
    public static final int PIXELSTEP = 20; // Kokiais žingsniais judama per paveikslėlį (pixeliais)
    public static final int COLORSTEP = 45000; // Kas kiek eina naujas spalvos ruožas (ColorBinas)
    public static final int MAXDARKNESS = 40; // Maksimalus tamsumas
    public static final int INFINITY = 2140000000; // Tiesiog Maximalus skaičius

}
