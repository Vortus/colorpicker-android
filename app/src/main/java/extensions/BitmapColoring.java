/*
Čia tiesiog pagalbinės funkcijos paveikslėliui
*/

package extensions;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;

public class BitmapColoring {

    // Bitmapo kampų suapvalinimas
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    // Gaunam RGB kaip masyvą
    public static int[] getRGBArr(int pixel) {
        int red = (pixel >> 16) & 0xff;
        int green = (pixel >> 8) & 0xff;
        int blue = (pixel) & 0xff;
        return new int[]{red,green,blue};

    }

    // Spalvų atstumas, atstumas tarp dviejų taškų
    public static double colorDifference(int colorA, int colorB){
        int rgbA[] = getRGBArr(colorA);
        int rgbB[] = getRGBArr(colorB);
        return Math.sqrt(Math.pow(rgbB[0] - rgbA[0], 2) +
                         Math.pow(rgbB[1] - rgbA[1], 2) +
                         Math.pow(rgbB[2] - rgbA[2], 2));
    }

    // Spalvos šviesumas
    public static double getLum(int color){
        int rgbArr[] = getRGBArr(color);
        return 0.2126 * rgbArr[0] + 0.7152 * rgbArr[1] + 0.0722 * rgbArr[2];
    }

    // Spalva į hex kodą
    public static String getHex(int color){
        return "#" + Integer.toHexString(color).substring(2).toUpperCase();
    }

}