/*
Čia yra apskaičiuojama ta trispalvė naudojant ColorBinus.
*/

package extensions;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImageHandler {

    // Kintamieji
    private int WIDTH; // Plotis bitmapo
    private int HEIGHT; // Aukštis bitmapo
    private List<RGBColorBin> colorBins; // Spalvų ruožai

    // Pagrindinis konstruktorius, naudojantis ColorBinus
    public ImageHandler(Bitmap image) throws Exception {
        this.WIDTH = image.getWidth(); // Nustatom ploti
        this.HEIGHT = image.getHeight(); // Nustatom aukšti
        colorBins = new ArrayList<>(); // Sukuriam naują sarašą ColorBinų

        for(int i = Variables.HEXSTART; i >= Variables.HEXEND; i-= Variables.COLORSTEP) // Generuojam ColorBinus(Spalvų ruožus)
            colorBins.add(new RGBColorBin(i)); // Idedam sugeneruotą ColorBiną į sarašą

        // Žiūrim per visą paveikslėlį
        for(int y = 0; y < HEIGHT; y += Variables.PIXELSTEP){ // Į Apačią
            for(int x = 0; x < WIDTH; x += Variables.PIXELSTEP){ // Į dešinę
                int rgb = image.getPixel(x, y); // Gaunam pixelio spalvą

                // Spalvos priskyrimas ColorBinui
                int i = 0, j = colorBins.size() - 1, index = 0;
                double minDifferenceValue = Variables.INFINITY;

                while(i < j){ // Einam per visus ColorBinus, ieškom artimiausio ColorBino

                    double diffA = BitmapColoring.colorDifference(colorBins.get(i).getColor(), rgb); // Gaunam skirtumą
                    double diffB = BitmapColoring.colorDifference(colorBins.get(j).getColor(), rgb); // Gaunam skirtumą

                    if(diffA < minDifferenceValue){ // Žiurim ar mažesnis už mažiausią
                        minDifferenceValue = diffA;
                        index = i;
                    }

                    if(diffB < minDifferenceValue){ // Žiurim ar mažesnis už mažiausią
                        minDifferenceValue = diffB;
                        index = j;
                    }

                    // Judam toliau
                    i++;
                    j--;
                }

                colorBins.get(index).increaseStoredCount(); // Įdedam spalvą į artimiausią ColorBiną
            }
        }

        Collections.sort(colorBins); // Surikiuojam ColorBinus
    }

    // Gaunam spalvą iš ColorBino
    public int getColor(int index){ // Gaunam spalvą iš pasirinkto ColorBino
        return colorBins.get(index).getColor();
    }

}
