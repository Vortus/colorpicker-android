/*
Čia apibrėžiamas kas yra ColorBinas
 */

package extensions;

public class RGBColorBin implements Comparable<RGBColorBin> {

    // Kintamieji
    private int COLOR; // Spalva
    private int storedCount; // Kiek spalvų yra viduje

    // Pagrindinis Konstruktorius
    public RGBColorBin(int COLOR){ // Konstruktorius
        this.COLOR = COLOR;
    }

    public void increaseStoredCount() {
        this.storedCount++;
    } // Funkcija padidinanti spalvų kieki vienetu

    public int getStoredCount() {
        return storedCount;
    } // Funkcija gražinanti laikomą spalvų kiekį

    public int getColor(){
        return COLOR;
    } // Funkcija gražinanti spalvą

    @Override // Funkcija palyginanti du ColorBinus
    public int compareTo(RGBColorBin another) { // Apibrėžiam kaip lyginti su kitu ColorBinu
        return another.getStoredCount() - getStoredCount();
    }

}
