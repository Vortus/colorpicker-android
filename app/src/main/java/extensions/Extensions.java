package extensions;

import android.app.Activity;
import android.view.WindowManager;

public class Extensions {

    public static void setFullscreen(Activity activity){
        activity.setTitle("  " + activity.getTitle()); // Prastumia tekstą truputį į dešinę
        activity.getWindow().setFlags( // Padaro fullscreen
                WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
